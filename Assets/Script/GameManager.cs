﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public TextMeshProUGUI life;
    public TextMeshProUGUI golds;
    public int gold;
    public GameObject gameOver;
    public GameObject spawn;
    private bool spawnIA;
    public int PV;

    // Start is called before the first frame update
    void Start()
    {
        gameOver.SetActive(false);
        PV = 30;
        spawnIA = true;
        gold = 75;
        Time.timeScale = 1;

        StartCoroutine(SpawnIA());
    }

    // Update is called once per frame
    void Update()
    {
        life.text = "LIFE : " + PV;
        golds.text = "GOLD : " + gold.ToString(); 
        if (PV == 0)
        {
            Time.timeScale = 0;
            gameOver.SetActive(true);
        }
    }

    public void GameOver()
    {
        gameOver.SetActive(false);
        SceneManager.LoadScene("Game1");
    }

    IEnumerator SpawnIA()
    {
        while (spawnIA == true)
        {
            yield return new WaitForSeconds(2f);
            GameObject enemy1 = Spawner.SharedInstance.GetPooledObject("IA");
            if (enemy1 != null)
            {
                enemy1.transform.position = spawn.transform.position;
                enemy1.transform.rotation = spawn.transform.rotation;
                enemy1.SetActive(true);
            }
        }
    }
}

