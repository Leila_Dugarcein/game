﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TowerSetUp : MonoBehaviour
{
    public Button crystalTower, guardTower, fireTower, laserTower, canonTower; 
    
    public GameObject buttonPanel;
    public GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        
        //Set tous les boutons off au lancement car pas assez de points
         Button[] button = buttonPanel.gameObject.GetComponentsInChildren<Button>();
         foreach (Button btn in button)
             btn.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        SetTowerTrue();
    }

    //Activation / Désactivation des boutons des tourelles
    public void SetTowerTrue()
    {
        if (gameManager.gold >= 50)
        {
            crystalTower.interactable = true;
        }
        else
        {
            crystalTower.interactable = false;
        }
        if (gameManager.gold >= 75)
        {
            guardTower.interactable = true;
        }
        else
        {
            guardTower.interactable = false;
        }
        if (gameManager.gold >= 100)
        {
            fireTower.interactable = true;
        }
        else
        {
            fireTower.interactable = false;
        }
        if (gameManager.gold >= 150)
        {
            canonTower.interactable = true;
        }
        else
        {
            canonTower.interactable = false;
        }
        if (gameManager.gold >= 200)
        {
            laserTower.interactable = true;
        }
        else
        {
            laserTower.interactable = false;
        }
    }

}
