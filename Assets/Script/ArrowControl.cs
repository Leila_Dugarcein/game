﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowControl : MonoBehaviour
{
    private Transform target;
    public float speed = 10f;
    public GameObject impactEffect;
    public Transform test;

    public float explosion = 0f;

    public int damageBullet;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if(target == null) // suppr missile
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distFrame = speed * Time.deltaTime;

        if(dir.magnitude <= distFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distFrame, Space.World);
        transform.LookAt(target);
    }

    // Setting damage to target
    void Damage(Transform monster)
    {
        IA x = monster.GetComponent<IA>();
        x.HP(damageBullet);
    }

    //Implémentation dégats des munitions sur les ennemis + effect
    void HitTarget()
    {
        GameObject effect = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effect, 3f);

        if(explosion > 0)
        {
            Explode();
        }
        else
        {
            Damage(target);
        }

      /*  IAManager = GameObject.Find("IA(Clone)").GetComponent<IA>(); // L.C // Leila trouves une solution pour éviter de use GO.Find
        test = target; // L.C
        if (test == IAManager.transform) // L.C
        {
            IAManager.HP(); // Leila code = L.C (info pour correction Toji)
            Debug.Log("DDDDD");
        }*/
       // Destroy(target.gameObject);
        Destroy(gameObject);
    }

    // Mise en place explosion zone (bullet)
    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosion); // Apparition d'une sphere qui récup les collider

        foreach(Collider collider in colliders)
        {
            if(collider.tag == "IA")
            {
                Damage(collider.transform);
            }
        }
    }

    // Créer une zone pour voir le radius d'effet de l'explosion (bullet)
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, explosion);
    }
}
