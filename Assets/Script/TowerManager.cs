﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Personnalisation des towers (attaché à TowerSetUp)
/// </summary>
[System.Serializable]
public class ConstructTowerPrice
{
    public GameObject towerPref;
    public int priceTower;
}

public class TowerManager : MonoBehaviour
{
   
    private Transform target; // private car le changement de cible est automatisé
    public float range; // range du GizmoSphere
    private IA ia;

    public string enemyTag = "IA";

    //tmp restant avant de tirer
    public float fireRate = 1f;
    private float fireCountDown = 0f; 

    public GameObject arrowPref; // prefab munition
    public Transform firePoint; // Départ des munitions

    [Header("ArrowTower")]

    public bool arrowTowerOn;

    [Header("Canon Tower")]

    public bool canonON;
    public Transform partToRotate; // Canon qui tourne
    public float turnSpeed = 5f; // vitesse de rota

    [Header("Laser Tower")]

    public LineRenderer lineLaser;
    public ParticleSystem frostEffect;
    public bool lasrOn; // Activation sur Unity pour le laserPref
    public bool snipeLaserOn;
    public float damageInTime = 10f;
    public float slow = 0.5f;

    public void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f); //Pour éviter la surchage de l'update on lance la fonction comme cela

    }

    public void Update()
    {
        if (target == null)
        {
            // Désactivation du laser
            if((lasrOn == true || snipeLaserOn == true) && lineLaser.enabled)
            {
                lineLaser.enabled = false;
                frostEffect.Stop();
            }
            return;
        }
            

        if(lasrOn == true || snipeLaserOn == true)
        {
            LaserShoot();
           
        }
        else
        {
            if (fireCountDown <= 0f)
            {
                Shoot();
                fireCountDown = 1 / fireRate; //maj délai
            }

            fireCountDown -= Time.deltaTime;

            // Activation rotation seulement du canon
            if (canonON)
                LockOnTarget();
            else
                return;
        }
    }

    public void LaserShoot()
    {
        ia.HP(damageInTime * Time.deltaTime);
        ia.Slow(slow);

        // Activation du laser
        if (lineLaser.enabled == false)
        {
            lineLaser.enabled = true;
            frostEffect.Play();
        }

        // Set des positions du laser (line renderer position index)
        lineLaser.SetPosition(0, firePoint.position);
        lineLaser.SetPosition(1, target.position);

        // Direction system particles
        Vector3 dir = firePoint.transform.position - target.transform.position;
        frostEffect.transform.rotation = Quaternion.LookRotation(dir);
        frostEffect.transform.position = target.position; //+ dir.normalized * 0.1f; // dir.normalize * rayon de l'ennemi
    }

    // Instance de tir
    void Shoot()
    {
        GameObject arrowGo = (GameObject)Instantiate(arrowPref, firePoint.position, firePoint.rotation);
        ArrowControl arrow = arrowGo.GetComponent<ArrowControl>(); // Liaison ArrowControll 

        if(arrow != null)
        {
            arrow.Seek(target);
        }
    }

    // Update de la target / focus l'ennemi le plus proche 
    //une fois que l'ennemi focus sort de la zone
    public void UpdateTarget()
    {
        GameObject[] ennemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach(GameObject enemy in ennemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, 
                enemy.transform.position);

            if(distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if(nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            ia = target.GetComponent<IA>();
        }
        else
        {
            target = null;
        }
    }

    //Rotation du canon vers l'ennemi cible
    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation
            , Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    //Zone de détection (sphere)
    private void OnDrawGizmosSelected()
    {
        if(arrowTowerOn)
        {
            range = 3f;
            Gizmos.DrawWireSphere(transform.position, range);
        }
        else if(canonON)
        {
            range = 2f;
            Gizmos.DrawWireSphere(transform.position, range);
        }
        else if (lasrOn)
        {
            range = 2.5f;
            Gizmos.DrawWireSphere(transform.position, range);
        }
        else if (snipeLaserOn)
        {
            range = 5f;
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }
}
