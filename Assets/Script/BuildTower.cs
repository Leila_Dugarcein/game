﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTower : MonoBehaviour
{
   /* public GameObject crystalTowerPref, guardTowerPref,
        fireTowerPref, laserTowerPref, canonTowerPref;*/

    private bool tower = false;

    [Header("General")]

    public GameManager gameManager;

    public Material placeTower;
    public Material placeColor;
    public Material placeError;

    public PlacingTower placingTower;

    [Header("Towers")]

    public ConstructTowerPrice crystalTowerPref;
    public ConstructTowerPrice guardTowerPref;
    public ConstructTowerPrice laserTowerPref;
    public ConstructTowerPrice fireTowerPref;
    public ConstructTowerPrice canonTowerPref;

    // Pour changer la pos.y pour pas que les towers s'instantiate au centre du GO
    [Header("Position.Y")]

    public Vector3 positionY;

    public void OnMouseOver()
    {
        if (!tower)
            this.GetComponent<MeshRenderer>().material = placeColor;
        else
            this.GetComponent<MeshRenderer>().material = placeError;

    }

    public void OnMouseExit()
    {
        this.GetComponent<MeshRenderer>().material = placeTower;
    }

    // Systeme de construction des tower
    public void OnMouseUp()
    {
        
        if (!tower)
        {
            if (placingTower.CT == true)
            {
                if (gameManager.gold < crystalTowerPref.priceTower)
                {
                    Debug.Log("Pas assez d'or");
                    return;
                }
                else
                {
                    Instantiate(crystalTowerPref.towerPref, transform.position + positionY, Quaternion.identity);
                    tower = true;
                    gameManager.gold = gameManager.gold -= crystalTowerPref.priceTower;
                }
            }
            else if (placingTower.GT == true)
            {
                if (gameManager.gold < guardTowerPref.priceTower)
                {
                    Debug.Log("Pas assez d'or");
                    return;
                }
                else
                {
                    Instantiate(guardTowerPref.towerPref, transform.position + positionY, Quaternion.identity);
                    tower = true;
                    gameManager.gold = gameManager.gold -= guardTowerPref.priceTower;
                }
            }
            else if (placingTower.FT == true)
            {
                if (gameManager.gold < fireTowerPref.priceTower)
                {
                    Debug.Log("Pas assez d'or");
                    return;
                }
                else
                {
                    Instantiate(fireTowerPref.towerPref, transform.position + positionY, Quaternion.identity);
                    tower = true;
                    gameManager.gold = gameManager.gold -= fireTowerPref.priceTower;
                }
            }
            else if (placingTower.LT == true)
            {
                if (gameManager.gold < laserTowerPref.priceTower)
                {
                    Debug.Log("Pas assez d'or");
                    return;
                }
                else
                {
                    Instantiate(laserTowerPref.towerPref, transform.position + positionY, Quaternion.identity);
                    tower = true;
                    gameManager.gold = gameManager.gold -= laserTowerPref.priceTower;
                }
            }
            else if (placingTower.RT == true)
            {
                if (gameManager.gold < canonTowerPref.priceTower)
                {
                    Debug.Log("Pas assez d'or");
                    return;
                }
                else
                {
                    Instantiate(canonTowerPref.towerPref, transform.position + positionY, Quaternion.identity);
                    tower = true;
                    gameManager.gold = gameManager.gold -= canonTowerPref.priceTower;
                }
            }
            else if (!placingTower.RT && !placingTower.CT && !placingTower.LT &&
                !placingTower.GT && !placingTower.FT)
                return;
        }
    }
}
