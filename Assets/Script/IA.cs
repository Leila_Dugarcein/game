﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IA : MonoBehaviour
{
  
    public float speed = 0.7f;
    public float speedSlow;

    private GameManager gameManager;

    public Transform[] target;
    private int waypointindex;
    private NavMeshAgent agent;

    public float PvIA;

    void Start()
    {
        speedSlow = speed;
        PvIA = 100;

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        agent = GetComponent<NavMeshAgent>();
        target = new Transform[WayPoints.points.Length];
        target[0] = WayPoints.points[0];
        waypointindex = 0;
        agent.destination = target[waypointindex].position;
    }

    public void Update()
    {
        speedSlow = speed;

        agent.speed = speedSlow;
        
        if (agent.remainingDistance < 0.1f)
        {
            NextPoints();
        }
        if (waypointindex == WayPoints.points.Length)
        {
            gameManager.PV = gameManager.PV - 5;
            waypointindex = 0;
            PvIA = 100;
            gameObject.SetActive(false);
        }

        /*   if(PvIA == 0) 
           {
               gameManager.gold = gameManager.gold + 10;
               waypointindex = 0;
               PvIA = 100;
               gameObject.SetActive(false);
           }*/
    }

    private void NextPoints()
    {
        waypointindex++;
        if (waypointindex < WayPoints.points.Length)
        {
            target[waypointindex] = WayPoints.points[waypointindex];
            agent.destination = target[waypointindex].position;
        }
    }

    // Vie - damage (ArrowControl) 
    public void HP(float amount)
    {
        PvIA -= amount;

        if (PvIA <= 0)
        {
            Die();
        }
    }

    // Mort d'un ennemi // C.T
    public void Die()
    {
        gameManager.gold += 25;
        Destroy(gameObject);
    }

    // Tentative pour ralentir les mob ...// code thibault // C.T
    public void Slow(float slow)
    {
        speedSlow = speed * (1f - slow);
        agent.speed = speedSlow;
        Debug.Log("freezeOn");
    }
}
