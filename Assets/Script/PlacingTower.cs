﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// A class for Setting Button Activation
public class PlacingTower : MonoBehaviour
{
    public bool CT = false;
    public bool GT = false;
    public bool FT = false;
    public bool LT = false;
    public bool RT = false;
  
    public void OnButtonCT()
    {
        CT = true;
        GT = false;
        FT = false;
        LT = false;
        RT = false;

        Debug.Log("CrystaalTower");
    }
    public void OnButtonGT()
    {
        CT = false;
        GT = true;
        FT = false;
        LT = false;
        RT = false;

        Debug.Log("GuardTower");
    }
    public void OnButtonFT()
    {
        CT = false;
        GT = false;
        FT = true;
        LT = false;
        RT = false;

        Debug.Log("FireTower");
    }
    public void OnButtonLT()
    {
        CT = false;
        GT = false;
        FT = false;
        LT = true;
        RT = false;

        Debug.Log("LaserTower");
    }
    public void OnButtonRT()
    {
        CT = false;
        GT = false;
        FT = false;
        LT = false;
        RT = true;

        Debug.Log("CanonTower");
    }

}
